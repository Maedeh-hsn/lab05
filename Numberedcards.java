public class NumberedCards extends ColorCard {
    private int number;

    public NumberedCards(Colors color, int number) {
        super(color);
        this.number = number;
    }

    public int getNumber() {
        return this.number;
    }

    public boolean canPlay(UnoCard floorCard) {
        if (floorCard instanceof NumberedCards) {
            NumberedCards card = (NumberedCards) floorCard;
            if (this.color == card.color || this.number == card.number) {
                return true;
            }
        }
        if (floorCard instanceof ColorCard) {
            ColorCard card = (ColorCard) floorCard;
            if (this.color == card.color) {
                return true;
            }
        }
        return false;
    }

    public String toString() {
        return "Color: " + this.color + ", Color " + this.number;
    }

}
