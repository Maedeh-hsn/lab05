public abstract class ColorCard implements UnoCard {
    protected Colors color;

    public ColorCard(Colors color) {
        this.color = color;
    }

}