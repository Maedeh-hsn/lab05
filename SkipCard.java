public class SkipCard extends ColorCard {

    public SkipCard(Colors color) {
        super(color);
    }

    public boolean canPlay(UnoCard floorCard) {
        if (floorCard instanceof ColorCard) {
            ColorCard card = (ColorCard) floorCard;
            return this.color == card.color; // ) {
            // return true;
            // }
        }
        return false;

    }

    public String toString() {
        return "Color: " + this.color;
    }
}
