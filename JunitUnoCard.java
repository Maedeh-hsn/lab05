
import static org.junit.jupiter.api.Assertions.*;
import org.junit.Test;
import java.util.ArrayList;

public class JunitUnoCard {

    @Test
    public void test_wildCards() {
        Deck deck = new Deck();

        ArrayList<UnoCard> cards = deck.getDeck();
        int count = 0;
        for (UnoCard card : cards) {
            if ((card instanceof WildCard) || (card instanceof WildPickup4))
                count++;
        }
        assertEquals(count, 8);
    }

    @Test
    public void test_NumberedCards() {
        Deck deck = new Deck();

        ArrayList<UnoCard> cards = deck.getDeck();
        int count = 0;
        for (UnoCard card : cards) {
            if (card instanceof NumberedCards)
                count++;
        }
        assertEquals(count, 80);
    }

    @Test
    public void test_SkipCard() {
        Deck deck = new Deck();

        ArrayList<UnoCard> cards = deck.getDeck();
        int count = 0;
        for (UnoCard card : cards) {
            if (card instanceof SkipCard)
                count++;
        }
        assertEquals(count, 2);
    }

    @Test
    public void test_ReverseCard() {
        Deck deck = new Deck();

        ArrayList<UnoCard> cards = deck.getDeck();
        int count = 0;
        for (UnoCard card : cards) {
            if (card instanceof ReverseCard)
                count++;
        }
        assertEquals(count, 2);
    }

    @Test
    public void test_Pickup2Card() {
        Deck deck = new Deck();

        ArrayList<UnoCard> cards = deck.getDeck();
        int count = 0;
        for (UnoCard card : cards) {
            if (card instanceof Pickup2Card)
                count++;
        }
        assertEquals(count, 2);
    }

    @Test
    public void test_addToDeck() {
        Deck deck = new Deck();

        ArrayList<UnoCard> cards = deck.getDeck();
        int expected = cards.size() + 1;

        deck.addToDeck(new Pickup2Card(Colors.Blue));
        int count = cards.size();

        assertEquals(count, expected);
    }

    @Test
    public void test_draw() {
        Deck deck = new Deck();

        ArrayList<UnoCard> cards = deck.getDeck();
        int expected = cards.size() - 1;

        deck.draw();
        int count = cards.size();

        assertEquals(count, expected);
    }

}
