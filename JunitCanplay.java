import static org.junit.jupiter.api.Assertions.*;
import org.junit.Test;

public class JunitCanplay {

    @Test
    public void canPlay_WildCard_NumberedCards() {
        WildCard wildCard = new WildCard();

        NumberedCards numberedCards = new NumberedCards(Colors.Green, 1);

        boolean result = wildCard.canPlay(numberedCards);

        assertEquals(result, true);
    }

    @Test
    public void canPlay_SkipCard_NumberedCards() {
        WildCard wildCard = new WildCard();

        SkipCard skipCard = new SkipCard(Colors.Green);

        boolean result = wildCard.canPlay(skipCard);

        assertEquals(result, true);
    }

    @Test
    public void canPlay_NumberedCards_NumberedCards1() {
        NumberedCards numberedCards1 = new NumberedCards(Colors.Green, 5);

        NumberedCards numberedCards2 = new NumberedCards(Colors.Green, 1);

        boolean result = numberedCards1.canPlay(numberedCards2);

        assertEquals(result, true);
    }

    @Test
    public void canPlay_NumberedCards_NumberedCards2() {
        NumberedCards numberedCards1 = new NumberedCards(Colors.Red, 5);

        NumberedCards numberedCards2 = new NumberedCards(Colors.Green, 1);

        boolean result = numberedCards1.canPlay(numberedCards2);

        assertEquals(result, false);
    }

    @Test
    public void canPlay_SkipCard_NumberedCards1() {
        SkipCard skipCard = new SkipCard(Colors.Green);

        NumberedCards numberedCards = new NumberedCards(Colors.Green, 1);

        boolean result = skipCard.canPlay(numberedCards);

        assertEquals(result, true);
    }

    @Test
    public void canPlay_SkipCard_NumberedCards2() {
        SkipCard skipCard = new SkipCard(Colors.Blue);

        NumberedCards numberedCards = new NumberedCards(Colors.Green, 1);

        boolean result = skipCard.canPlay(numberedCards);

        assertEquals(result, false);
    }

    @Test
    public void canPlay_SkipCard_NumberedCards3() {
        SkipCard skipCard = new SkipCard(Colors.Blue);

        NumberedCards numberedCards = new NumberedCards(Colors.Green, 1);

        boolean result = numberedCards.canPlay(skipCard);

        assertEquals(result, false);
    }

    @Test
    public void canPlay_ReverseCard_NumberedCards1() {
        ReverseCard reverseCard = new ReverseCard(Colors.Yellow);

        NumberedCards numberedCards = new NumberedCards(Colors.Yellow, 1);

        boolean result = numberedCards.canPlay(reverseCard);

        assertEquals(result, true);
        ;
    }

    @Test
    public void canPlay_ReverseCard_NumberedCards2() {
        ReverseCard reverseCard = new ReverseCard(Colors.Green);

        NumberedCards numberedCards = new NumberedCards(Colors.Green, 1);

        boolean result = numberedCards.canPlay(reverseCard);

        assertEquals(result, true);
    }
}