public class ReverseCard extends ColorCard {
    private boolean gameDirection;

    public ReverseCard(Colors color) {
        super(color);
        this.gameDirection = true;
    }

    public boolean getGameDirection() {
        return this.gameDirection;
    }

    public boolean canPlay(UnoCard floorCard) {
        if (floorCard instanceof ColorCard) {
            ColorCard card = (ColorCard) floorCard;
            if (this.color == card.color) {
                return true;
            }
        }
        return false;
    }

    public String toString() {
        return "Color: " + this.color;
    }
}
