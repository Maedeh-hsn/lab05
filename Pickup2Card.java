public class Pickup2Card extends ColorCard {
    private int handCardNum;

    public Pickup2Card(Colors color) {
        super(color);
    }

    public int getHandCardNum() {
        return this.handCardNum;
    }

    public boolean canPlay(UnoCard floorCard) {
        if (floorCard instanceof ColorCard) {
            ColorCard card = (ColorCard) floorCard;
            if (this.color == card.color) {
                return true;
            }
        }
        return false;
    }

    public String toString() {
        return "Color: " + this.color;
    }
}
