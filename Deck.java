import java.util.ArrayList;

public class Deck {

    private ArrayList<UnoCard> deck = new ArrayList<UnoCard>();

    public Deck() {

        for (int i = 0; i < 4; i++) {
            this.deck.add(new WildCard());
        }

        for (int i = 0; i < 4; ++i) {
            this.deck.add(new WildPickup4());
        }

        for (int i = 0; i <= 9; ++i) {
            this.deck.add(new NumberedCards(Colors.Blue, i));
            this.deck.add(new NumberedCards(Colors.Blue, i));

            this.deck.add(new NumberedCards(Colors.Red, i));
            this.deck.add(new NumberedCards(Colors.Red, i));

            this.deck.add(new NumberedCards(Colors.Green, i));
            this.deck.add(new NumberedCards(Colors.Green, i));

            this.deck.add(new NumberedCards(Colors.Yellow, i));
            this.deck.add(new NumberedCards(Colors.Yellow, i));
        }

        this.deck.add(new SkipCard(Colors.Blue));
        this.deck.add(new SkipCard(Colors.Red));

        this.deck.add(new ReverseCard(Colors.Blue));
        this.deck.add(new ReverseCard(Colors.Red));

        this.deck.add(new Pickup2Card(Colors.Blue));
        this.deck.add(new Pickup2Card(Colors.Red));
    }

    public ArrayList<UnoCard> getDeck() {
        return this.deck;
    }

    public void addToDeck(UnoCard card) {
        this.deck.add(card);
        System.out.println(this.deck + " added to the deck.");
    }

    public UnoCard draw() {
        UnoCard top = this.deck.get(0);
        this.deck.remove(top);
        System.out.println(this.deck + " was removed.");
        return top;
    }

}
